# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
USE_RUBY="ruby21"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_EXTRADOC=""

RUBY_FAKEGEM_TASK_TEST=""

RUBY_FAKEGEM_GEMSPEC="${PN}.gemspec"

inherit ruby-fakegem

DESCRIPTION="Interface-agnostic context and state-aware agent"
HOMEPAGE="https://github.com/chendo/cora"

LICENSE="freedist"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

# Appears to require a test? () entry, otherwise a syntax error is generated :(
ruby_add_bdepend "
	dev-ruby/guard-rspec
	dev-ruby/rake
	dev-ruby/rspec
"

ruby_add_rdepend "
	dev-ruby/geocoder
"

all_ruby_prepare() {
	sed -i \
		-e '/git ls-files/d' \
		"${RUBY_FAKEGEM_GEMSPEC}" || die
}
