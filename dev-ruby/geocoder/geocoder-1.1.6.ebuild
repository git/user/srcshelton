# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
USE_RUBY="ruby21"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_EXTRADOC="README.md"

RUBY_FAKEGEM_TASK_TEST="test"

inherit ruby-fakegem

DESCRIPTION="Complete Ruby geocoding solution"
HOMEPAGE="http://www.rubygeocoder.com/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
